import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_pelis/models/models.dart';
import 'package:flutter_pelis/models/search_response.dart';
import 'package:http/http.dart' as http;

class MoviesProvider extends ChangeNotifier {

  final String _apiKey = 'ff5bd1b2b44adacc0c3796cfd7904f31';
  final String _baseUrl = 'api.themoviedb.org';
  final String _language = 'es-ES';

  List<Movie> onDisplayMovies = [];
  List<Movie> popularMovies = [];

  Map<int, List<Cast>> movieCast = {};

  int _popularMoviesPage = 0;

  MoviesProvider() {
    this.getNowPlayingMovies();
    this.getPopularMovies();
  }

  getNowPlayingMovies() async {
    final jsonData = await _getJsonData('3/movie/now_playing', 1);
    final NowPlayingResponse decodedData =
        NowPlayingResponse.fromJson(jsonData);
    this.onDisplayMovies = decodedData.results;

    notifyListeners();
  }

  getPopularMovies() async {
    _popularMoviesPage++;
    final jsonData = await _getJsonData('3/movie/popular', _popularMoviesPage);
    final PopularResponse popularResponse = PopularResponse.fromJson(jsonData);
    this.popularMovies = [...this.popularMovies, ...popularResponse.results];

    notifyListeners();
  }

  Future<List<Cast>> getMovieCast(int movieId) async {
    if (movieCast.containsKey(movieId)) {
      return movieCast[movieId]!;
    }

    final jsonData = await _getJsonData('3/movie/$movieId/credits');

    final CreditsResponse creditsResponse = CreditsResponse.fromJson(jsonData);
    movieCast[movieId] = creditsResponse.cast;
    return movieCast[movieId]!;
  }

  Future<String> _getJsonData(String uri, [int page = 1]) async {
    final url = Uri.http(_baseUrl, uri, {
      'api_key': _apiKey,
      'language': _language,
      'page': '$page',
    });

    final response = await http.get(url);
    return response.body;
  }

  Future<List<Movie>> searchMovies(String query, [int page = 1]) async {
    final jsonData = await _searchJsonData('3/search/movie', query, page);

    final searchResponse = SearchResponse.fromJson(jsonData);
    return searchResponse.results;
  }

  Future<String> _searchJsonData(String uri, String query, [int page = 1]) async {
    final url = Uri.http(_baseUrl, uri, {
      'api_key': _apiKey,
      'language': _language,
      'page': '$page',
    });

    final response = await http.get(url);
    return response.body;
  }

}
