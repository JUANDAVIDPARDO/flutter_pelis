import 'package:flutter/material.dart';
import 'package:flutter_pelis/widgets/widgets.dart';

import '../models/models.dart';

class DetailsScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final Movie movie = ModalRoute.of(context)!.settings.arguments as Movie;

    return Scaffold(
        body: CustomScrollView(
      slivers: [
        _CustomAppBar(movie: movie),
        SliverList(
          delegate: SliverChildListDelegate([
            _PosterAndTitle(movie: movie,),
            const SizedBox(height: 15,),
            _Overview(movie: movie,),
            const SizedBox(
              height: 40,
            ),
            CastingCards(movieId: movie.id),
          ]),
        ),
      ],
    ));
  }
}

class _CustomAppBar extends StatelessWidget {
  final Movie movie;

  const _CustomAppBar({super.key, required this.movie});

  @override
  Widget build(BuildContext context) {
    return SliverAppBar(
      expandedHeight: 200,
      floating: false,
      pinned: true,
      flexibleSpace: FlexibleSpaceBar(
        background: FadeInImage(
          placeholder: const AssetImage('assets/loading.gif'),
          image: _getBackdropPath(movie),
          fit: BoxFit.cover,
        ),
        centerTitle: true,
        title: Container(
          alignment: Alignment.bottomCenter,
          color: Colors.black26,
          width: double.infinity,
          child: Padding(
            padding: const EdgeInsets.only(bottom: 20, left: 10, right: 10),
            child: Text(
              movie.title,
              style: const TextStyle(fontSize: 16),
            ),
          ),
        ),
        titlePadding: const EdgeInsets.all(0),
      ),
    );
  }

  ImageProvider _getBackdropPath(Movie movie) {
    if (movie.fullBackdropPath != null) {
      return NetworkImage(movie.fullBackdropPath);
    }
    return const AssetImage('assets/no-image.jpg');
  }
}

class _PosterAndTitle extends StatelessWidget {
  final Movie movie;

  const _PosterAndTitle({super.key, required this.movie});

  @override
  Widget build(BuildContext context) {
    final TextTheme textTheme = Theme.of(context).textTheme;
    final Size size =  MediaQuery.of(context).size;

    return Container(
      margin: const EdgeInsets.only(top: 20),
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: Row(
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(20),
            child: FadeInImage(
              placeholder: const AssetImage('assets/no-image.jpg'),
              image: _getPosterImage(movie),
              height: 150,
            ),
          ),
          const SizedBox(
            width: 20,
          ),
          ConstrainedBox(
            constraints: BoxConstraints(maxWidth: size.width - 190),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  movie.title,
                  style: textTheme.headline6,
                  overflow: TextOverflow.ellipsis,
                  maxLines: 2,
                ),
                Text(
                  movie.originalTitle,
                  style: textTheme.subtitle1,
                  overflow: TextOverflow.ellipsis,
                  maxLines: 2,
                ),
                Row(
                  children: [
                    StarsAverge(maxAverage: 10, average: movie.voteAverage),
                    const SizedBox(
                      width: 6,
                    ),
                    Text(
                      movie.voteAverage.toString(),
                      style: textTheme.caption,
                    ),
                  ],
                )
              ],
            ),
          )
        ],
      ),
    );
  }

  ImageProvider _getPosterImage(Movie movie) {
    if (movie.fullPosterImg != null) {
      return NetworkImage(movie.fullPosterImg);
    }
    return const AssetImage('assets/no-image.jpg');
  }
}

class _Overview extends StatelessWidget {
  final Movie movie;

  const _Overview({super.key, required this.movie});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 10),
      child: Text(
        movie.overview,
        style: Theme.of(context).textTheme.subtitle1,
        textAlign: TextAlign.justify,
      ),
    );
  }
}
