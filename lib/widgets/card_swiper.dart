import 'package:card_swiper/card_swiper.dart';
import 'package:flutter/material.dart';

import '../models/models.dart';

class CardSwiper extends StatelessWidget {

  final List<Movie> movies;

  const CardSwiper({required this.movies});

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;

    if (this.movies.isEmpty) {
      return Container(
        width: double.infinity,
        height: size.height * .5,
        child: const Center(
          child: CircularProgressIndicator(),
        ),
      );
    }

    return SizedBox(
      width: double.infinity,
      height: size.height * .5,
      child: Swiper(
        itemCount: movies.length,
        layout: SwiperLayout.STACK,
        itemHeight: size.height * .4,
        itemWidth: size.width * .6,
        itemBuilder: (_, index) {
          final movie = movies[index];

          return GestureDetector(
            onTap: () => Navigator.pushNamed(context, 'details',
                arguments: movie),
            child: ClipRRect(
                borderRadius: BorderRadius.circular(20),
                child: FadeInImage(
                  placeholder: const AssetImage('assets/no-image.jpg'),
                  image: _getPosterImage(movie),
                  fit: BoxFit.cover,
                )),
          );
        },
      ),
    );
  }

  ImageProvider _getPosterImage(Movie movie) {

    if ( movie.fullPosterImg != null ) {
      return NetworkImage(movie.fullPosterImg);
    }
     return const AssetImage('assets/no-image.jpg');
  }
}
