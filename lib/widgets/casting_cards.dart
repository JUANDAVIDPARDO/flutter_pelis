import 'package:flutter/material.dart';
import 'package:flutter_pelis/providers/movies_provider.dart';
import 'package:provider/provider.dart';

import '../models/models.dart';

class CastingCards extends StatelessWidget {
  final int movieId;

  const CastingCards({super.key, required this.movieId});

  @override
  Widget build(BuildContext context) {
    final moviesProvider = Provider.of<MoviesProvider>(context, listen: false);
    return FutureBuilder(
        future: moviesProvider.getMovieCast(movieId),
        builder: (_, AsyncSnapshot<List<Cast>> snapshot) {

          if (!snapshot.hasData) {
            return const SizedBox(width: 150, height: 180,);
          }

          final List<Cast> listCast = snapshot.data!.cast();

          return Container(
            width: double.infinity,
            height: 210,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Padding(
                  padding: EdgeInsets.symmetric(horizontal: 30),
                  child: Text(
                    "Casting",
                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.w600),
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                Expanded(
                  child: ListView.builder(
                    itemCount: listCast.length,
                    itemBuilder: (_, int index) => Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 10),
                      child: _CastCard(cast: listCast[index]),
                    ),
                    scrollDirection: Axis.horizontal,
                  ),
                )
              ],
            ),
          );
        });
  }
}

class _CastCard extends StatelessWidget {
  final Cast cast;

  const _CastCard({super.key, required this.cast});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 160,
      width: 90,
      child: Column(
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(10),
            child:  FadeInImage(
              fit: BoxFit.contain,
              height: 100,
              image: _getProfilePath(cast),
              placeholder: AssetImage('assets/no-image.jpg'),
            ),
          ),
          const SizedBox(
            height: 5,
          ),
          Text(
            cast.name,
            style: Theme.of(context).textTheme.labelMedium,
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
          ),
          Text(
            cast.character ?? '',
            style: Theme.of(context).textTheme.labelSmall,
            maxLines: 3,
            overflow: TextOverflow.ellipsis,
          ),
        ],
      ),
    );
  }

  ImageProvider _getProfilePath(Cast cast) {
    if (cast.fullProfilePath != null) {
      return NetworkImage(cast.fullProfilePath!);
    }
    return const AssetImage('assets/no-image.jpg');
  }
}
