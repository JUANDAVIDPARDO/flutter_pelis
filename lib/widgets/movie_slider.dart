import 'package:flutter/material.dart';

import '../models/models.dart';

class MovieSlider extends StatefulWidget {
  final String? name;
  final List<Movie> movies;
  final Function? onNextPage;

  const MovieSlider(
      {super.key, this.name, required this.movies, this.onNextPage});

  @override
  State<MovieSlider> createState() => _MovieSliderState();
}

class _MovieSliderState extends State<MovieSlider> {
  final ScrollController scrollController = ScrollController();

  @override
  void initState() {
    super.initState();

    scrollController.addListener(() {
      if (scrollController.position.pixels <=
          (scrollController.position.maxScrollExtent - 500) ) {
        return;
      }

      if (widget.onNextPage != null) {
        widget.onNextPage!();
      }
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (widget.movies.isEmpty) {
      return Container(
        width: double.infinity,
        height: 250,
        child: const Center(
          child: CircularProgressIndicator(),
        ),
      );
    }

    return Container(
      width: double.infinity,
      height: 250,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          if (widget.name != null)
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 30),
              child: Text(
                "${widget.name}",
                style:
                    const TextStyle(fontSize: 16, fontWeight: FontWeight.w600),
              ),
            ),
          const SizedBox(
            height: 10,
          ),
          Expanded(
              child: ListView.builder(
            controller: scrollController,
            scrollDirection: Axis.horizontal,
            itemCount: widget.movies.length,
            itemBuilder: (_, int index) =>
                _MoviePoster(movie: widget.movies[index]),
          ))
        ],
      ),
    );
  }
}

class _MoviePoster extends StatelessWidget {
  final Movie movie;

  const _MoviePoster({Key? key, required this.movie}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        width: 130,
        height: 190,
        margin: const EdgeInsets.symmetric(horizontal: 4),
        child: GestureDetector(
          onTap: () => Navigator.of(context)
              .pushNamed('details', arguments: movie),
          child: Column(
            children: [
              ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: SizedBox(
                    height: 180,
                    child: FadeInImage(
                      placeholder: const AssetImage('assets/no-image.jpg'),
                      image: _getPosterImage(movie),
                      fit: BoxFit.fill,
                    )),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8),
                child: Text(
                  movie.title,
                  style: const TextStyle(overflow: TextOverflow.ellipsis),
                  maxLines: 2,
                ),
              )
            ],
          ),
        ));
  }

  ImageProvider _getPosterImage(Movie movie) {
    if (movie.fullPosterImg != null) {
      return NetworkImage(movie.fullPosterImg);
    }
    return const AssetImage('assets/no-image.jpg');
  }
}
