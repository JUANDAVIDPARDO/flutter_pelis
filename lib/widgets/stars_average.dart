import 'package:flutter/material.dart';

class StarsAverge extends StatelessWidget {
  static const double _MAXSTARTS = 5;
  final double average;
  final double maxAverage;

  StarsAverge({Key? key, required this.maxAverage, required this.average})
      : super(key: key) {}

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        ..._getFilledStars(),
        ..._getHalfStar(),
        ..._getEmptyStars(),
      ],
    );
    ;
  }

  int _getNumberOfStars() {
    double unit = _MAXSTARTS / maxAverage;
    return (average * unit).floor();
  }

  List<Icon> _getFilledStars() {
    List<Icon> starIcons = [];
    for (var i = 1; i <= _getNumberOfStars(); i++) {
      starIcons.add(const Icon(Icons.star, color: Colors.amber));
    }

    return starIcons;
  }

  bool _haveRemainder() {
    double unit = _MAXSTARTS / maxAverage;
    return (average * unit) % 1 != 0;
  }

  List<Icon> _getHalfStar() {
    if (_haveRemainder()) {
      return [
        const Icon(
          Icons.star_half,
          color: Colors.amber,
        )
      ];
    }
    return [];
  }

  int _getNumberOfEmptyStars() {
    double unit = _MAXSTARTS / maxAverage;
    return (_MAXSTARTS - (average * unit)).floor();
  }

  List<Icon> _getEmptyStars() {
    List<Icon> starIcons = [];
    for (var i = 1; i <= _getNumberOfEmptyStars(); i++) {
      starIcons.add(const Icon(Icons.star_border_outlined, color: Color(0xffC0C2C9)));
    }

    return starIcons;
  }
}
